package com.travelworld.app.trips;

import org.springframework.data.jpa.repository.JpaRepository;


public interface CitiesRepository extends JpaRepository<Cities, Long> {

}
