package com.travelworld.app.trips;

import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name = "trips")
public class Trip {

	
	@Id
	private String id;

	
	@Column(nullable = false, length = 60)
	private String email;

	
	@Column(nullable = false, length = 30)
	private String firstName;

	
	@Column(nullable = false, length = 30)
	private String lastName;

	
	@Column(nullable = false)
	private String tripType;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "destination", referencedColumnName = "id")
	private Cities destination;

	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "origin", referencedColumnName = "id")
	private Cities origin;

	
	@Column(nullable = false)
	private String tripDate;

	
	@Column(nullable = false)
	private String contactNumber;

	
	@Transient
	private String originId;

	
	@Transient
	private String destinationId;


	
	public void generateId() {
		this.id = UUID.randomUUID().toString();
	}


	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTripType() {
		return tripType;
	}

	public void setTripType(String tripType) {
		this.tripType = tripType;
	}

	public Cities getDestination() {
		return destination;
	}

	public void setDestination(Cities destination) {
		this.destination = destination;
	}

	public Cities getOrigin() {
		return origin;
	}

	public void setOrigin(Cities origin) {
		this.origin = origin;
	}

	public String getTripDate() {
		return tripDate;
	}

	public void setTripDate(String tripDate) {
		this.tripDate = tripDate;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getOriginId() {
		return originId;
	}

	public void setOriginId(String originId) {
		this.originId = originId;
	}

	public String getDestinationId() {
		return destinationId;
	}

	public void setDestinationId(String destinationId) {
		this.destinationId = destinationId;
	}

	@Override
	public String toString() {
		return "Trip [id=" + id + ", email=" + email + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", tripType=" + tripType + ", destination=" + destination + ", origin=" + origin + ", tripDate="
				+ tripDate + ", contactNumber=" + contactNumber + "]";
	}

}
